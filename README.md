# GS Code

A custom color theme for VSCode

## Setup

First, clone or download the repository.

#### Install the theme

1. Create a folder in the extensions folder of VSCode, such as:

```bash
# On Linux
mkdir -p ~/.vscode/extensions/theme-gs-code
```

2. Copy `package.json` and the `theme/` folder to the extensions folder of VSCode:

```bash
# On Linux
~/.vscode/extensions/theme-gs-code

# On Windows
%USERPROFILE%\.vscode\extensions\theme-gs-code
<path-to-vscode>/resources/app/extensions/theme-gs-code # or
```

#### Copy settings/shortcuts/snippets

1. Copy the contents of `custom-config/` to the user settings folder of VSCode:

```bash
# On Linux
~/.config/Code/User

# On Windows
%APPDATA%\Code\User                 # or
<path-to-vscode>\data\user-data\User
```
